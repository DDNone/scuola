package com.journaldev.customlistview;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {


    public ArrayList<DataModel> dataModels;
    ListView listView;
    public static CustomAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        listView=(ListView)findViewById(R.id.list);


        dataModels= new ArrayList<>();

        //dati prelevati dal db carrello
        dataModels.add(new DataModel("Apple Pie", "Android 1.0", "1"));
        dataModels.add(new DataModel("Banana Bread", "Android 1.1", "2"));
        dataModels.add(new DataModel("Cupcake", "Android 1.5", "3"));
        dataModels.add(new DataModel("Donut","Android 1.6","4"));
        dataModels.add(new DataModel("Eclair", "Android 2.0", "5"));
        dataModels.add(new DataModel("Froyo", "Android 2.2", "8"));
        dataModels.add(new DataModel("Gingerbread", "Android 2.3", "9"));
        dataModels.add(new DataModel("Honeycomb","Android 3.0","11"));
        dataModels.add(new DataModel("Ice Cream Sandwich", "Android 4.0", "14"));
        dataModels.add(new DataModel("Jelly Bean", "Android 4.2", "16"));
        dataModels.add(new DataModel("Kitkat", "Android 4.4", "19"));
        dataModels.add(new DataModel("Lollipop","Android 5.0","21"));
        dataModels.add(new DataModel("Marshmallow", "Android 6.0", "23"));

        adapter= new CustomAdapter(dataModels,getApplicationContext());

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                DataModel dataModel= dataModels.get(position);

                Snackbar.make(view, dataModel.getName()+"\n"+" API: "+dataModel.getVersion_number(), Snackbar.LENGTH_LONG)
                        .setAction("No action", null).show();
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
