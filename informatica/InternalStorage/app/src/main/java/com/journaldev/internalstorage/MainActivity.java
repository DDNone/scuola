package com.journaldev.internalstorage;


import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends Activity {

    EditText textmsg;
    InternalStorageUtil isu= new InternalStorageUtil();
    static final int READ_BLOCK_SIZE = 100;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textmsg=(EditText)findViewById(R.id.editText1);
    }

    // write text to file
    public void WriteBtn(View v) {
        // add-write text into file
        try {
            Prova p = new Prova(5, "ciao");
            Prova p2 = new Prova(2, "prova2");
            isu.writeObject(this, "prova", p);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Read text from file
    public void ReadBtn(View v) {
        //reading text from file
        //try obj
        try {
            Prova p=(Prova)isu.readObject(this,"prova");
            textmsg.setText(p.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
