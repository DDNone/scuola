package com.journaldev.internalstorage;

import java.io.Serializable;

public class Prova implements Serializable {
    int n;
    String s;

    public Prova(int n, String s) {
        this.n = n;
        this.s = s;
    }

    @Override
    public String toString() {
        return "Prova{" +
                "n=" + n +
                ", s='" + s + '\'' +
                '}';
    }
}
