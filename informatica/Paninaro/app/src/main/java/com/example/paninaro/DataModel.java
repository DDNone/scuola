package com.example.paninaro;

public class DataModel {

    String name;
    //String type;
    String prezzo;

    String classe;

    public DataModel(String name, String prezzo, String classe) {
        this.name = name;
        //prezzo=prezzo.substring(0,4);//tolgo caratteri speciali
        this.prezzo = prezzo;
        this.classe=classe;
    }

    public String getClasse() {
        return classe;
    }

    public String getName() {
        return name;
    }

    public String getPrezzo() {
        return ""+prezzo+"";
    }
}
