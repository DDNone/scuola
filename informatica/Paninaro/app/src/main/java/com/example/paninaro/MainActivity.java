package com.example.paninaro;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {


    public ArrayList<DataModel> dataModels=null;
    ListView listView;
    public static CustomAdapter adapter;
    TextView debag;
    Button reload;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.list);
        this.reload=findViewById(R.id.btnReload);
        reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDati();
            }
        });
       try{
           this.getDati();
       }catch (Exception e) {//devo ancora prelevare i dati dal db
        e.printStackTrace();
       }

    }

    public void getDati(){
        try {
            PrelavaDati lg= new PrelavaDati(getApplicationContext(),this.listView);
            //EffettuaLogin lg2= new EffettuaLogin(getApplicationContext());
            lg.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




}
