package com.example.paninaro;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PrelavaDati extends AsyncTask {
    public String str;
    Context context;
    public JSONArray objresponse;
    static ArrayList<DataModel> cls = null;
    EditText txtUser, txtPw;
    ListView listView;
    public static CustomAdapter adapter;
    public ArrayList<DataModel> dataModels=new ArrayList<DataModel>();

    public PrelavaDati(Context context, ListView list) {
        this.context=context;
        this.listView=list;
        cls= new ArrayList<DataModel>();
        try {
            String var= "preleva dati(Context context)";
            this.context = context;
            this.txtUser=txtUser;
            this.txtPw= txtPw;
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected JSONArray doInBackground(Object[] objects) {
        String var= "doInBackground";
        try {
            loadPanini();

        }catch (Exception e){
            e.printStackTrace();
        }

        return objresponse;
    }

    public void loadPanini(){// funzia controllare context
        // Read Server Response indirizzo ip
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://panelpanini.altervista.org/preleva.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            //converting the string to json array object
                            objresponse = new JSONArray(response);
                            Control();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });
        Volley.newRequestQueue(context).add(stringRequest);
    }


    public void Control(){//trasformo json in obj
        try {

            String id, nome, p;
            JSONObject jAp;
            DataModel ap=null,ap2=null;
            double prezzo;

            int i;
            for ( i= 0; i < this.objresponse.length(); i++) {
                jAp = this.objresponse.getJSONObject(i);
                id = jAp.getString("idClasse");
                nome = jAp.getString("nome");
                p = jAp.getString("prezzo");
                ap = new DataModel(nome, p, id);
                cls.add(ap);
            }

            //avvio home aggiungendo parametri
            //Intent myIntent = new Intent(context, MainActivity.class);
            //myIntent.putExtra("data", cls);//invio ad home
            //context.startActivity(myIntent);

            //creazione lista
            double tot = 0;
            String remove;

            for (int ix=0;ix< cls.size();ix++){
                try {
                    ap = cls.get(ix);
                    ap2 = cls.get(ix + 1);

                }catch(Exception e){//caso finale
                    this.dataModels.add(ap);
                    try {
                        remove = ap.prezzo.substring(0, 3);
                        tot += Double.parseDouble(remove);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    e.printStackTrace();
                    ap2=new DataModel("", "", "");
                    dataModels.add(new DataModel("totale classe " + ap.classe + ":", "" + tot, ap.classe));
                    break;
                }

                if (ap.getClasse().equals( ap2.getClasse())) {
                    this.dataModels.add(ap);
                    try {
                        remove = ap.prezzo.substring(0, 3);
                        tot += Double.parseDouble(remove);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    this.dataModels.add(ap);
                    try {
                        remove = ap.prezzo.substring(0, 3);
                        tot += Double.parseDouble(remove);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    dataModels.add(new DataModel("totale classe " + ap.classe + ":", "" + tot, ap.classe));
                    tot=0;

                }

            }


            //this.dataModels=cls;
            //DataModel ap = null;


            //dataModels.add(new DataModel("totale classe " + ap.classe + ":", "" + tot, ap.classe));
            adapter = new CustomAdapter(dataModels, context);

            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    DataModel dataModel = dataModels.get(position);
                    Toast.makeText(context, "item ", Toast.LENGTH_SHORT).show();
                }
            });

        }catch(Exception e){
            e.printStackTrace();

        }
    }

}
