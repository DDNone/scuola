import java.net.*;
import java.io.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;


class GeocodingException extends Exception {
}

public class Geocoding {
    private String prefix = "http://maps.googleapis.com/maps/api/geocode/xml?address=";
    private String suffix = "&sensor=false";
    private String url;
    private String filename;
    private boolean saved = false;
    private boolean parsed = false;
    private double latitude;
    private double longitude;

    public Geocoding(String address, String filename) {
        URL server;
        HttpURLConnection service;
        BufferedReader input;
        BufferedWriter output;
        int status;
        String line;
        
        this.filename = filename;
        try {
            // costruzione dello URL di interrogazione del web-service
            url = prefix + URLEncoder.encode(address, "UTF-8") + suffix;
            server = new URL(url);
            service = (HttpURLConnection)server.openConnection();
            // impostazione header richiesta
            service.setRequestProperty("Host", "maps.googleapis.com");
            service.setRequestProperty("Accept", "application/xml");
            service.setRequestProperty("Accept-Charset", "UTF-8");
            // impostazione metodo di richiesta GET
            service.setRequestMethod("GET");
            // attivazione ricezione
            service.setDoInput(true);
            // connessione al web-service
            service.connect();
            // verifica stato risposta
            status = service.getResponseCode();
            if (status != 200) {
                return; // non OK
            }
            // aperture stream di ricezione da risorsa web
            input = new BufferedReader(new InputStreamReader(service.getInputStream(), "UTF-8"));
            // aperture stream per scrittura su file
            output = new BufferedWriter(new FileWriter(filename));
            // ciclo di lettura da web e scrittura su file
            while ((line = input.readLine()) != null) {
                output.write(line);
                output.newLine();
            }
            input.close();
            output.close();
            saved = true;
        }
        catch (IOException e) {
        }
    }

    private void parseXML() throws GeocodingException {
        if (!saved) {
            throw new GeocodingException();
        }
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            // costruzione albero DOM del file XML
            Document document = builder.parse(filename);
            // estrazione elemento radice
            Element root = document.getDocumentElement();
            // ricerca elemento "status"
            NodeList list = root.getElementsByTagName("status");
            if (list != null && list.getLength() > 0) {
                // verifica esito (valore elemento "status" = "OK")
                if (list.item(0).getFirstChild().getNodeValue(). equalsIgnoreCase("OK")) {
                    // ricerca elemento "location"
                    list = root.getElementsByTagName("location");
                    if (list != null && list.getLength() > 0) {
                        Element loc = (Element)list.item(0);
                        // ricerca elemento "lat"
                        NodeList lat = loc.getElementsByTagName("lat");
                        // conversione in valore numerico
                        latitude = Double.parseDouble(lat.item(0).
                        getFirstChild().getNodeValue());
                        // ricerca elemento "lng"
                        NodeList lng = loc.getElementsByTagName("lng");
                        // conversione in valore numerico
                        longitude = Double.parseDouble(lng.item(0).
                        getFirstChild().getNodeValue());
                        parsed = true;
                    }
                }
            }
        }
        catch (IOException e) {
            throw new GeocodingException();
        }
        catch (ParserConfigurationException e) {
            throw new GeocodingException();
        }
        catch (SAXException e) {
            throw new GeocodingException();
        }
    }

    public double getLongitude() throws GeocodingException {
        if (!saved) {
            throw new GeocodingException();
        }
        if (!parsed) {
            parseXML();
        }
        return longitude;
    }

    public double getLatitude() throws GeocodingException {
        if (!saved) {
            throw new GeocodingException();
        }
        if (!parsed) {
            parseXML();
        }
        return latitude;
    }

    public static void main(String args[]) {
        Geocoding zanichelli = new Geocoding("Via Irnerio, 34 Bologna, Italia", "file.xml");
        try {
            System.out.println("(" + zanichelli.getLatitude() + ";" + zanichelli.getLongitude() + ")");
        }
        catch (GeocodingException e) {
            System.out.println("Errore invocazione web-service!");
        }
    }
}
