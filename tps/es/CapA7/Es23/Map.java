import java.net.*;
import java.io.*;


public class Map {
    private String prefix = "http://maps.googleapis.com/maps/api/staticmap?";
    private String url;
    private boolean saved = false;

    public Map(double latitude, double longitude, int zoom, int width, int height, String filename) {
        URL server;
        HttpURLConnection service;
        BufferedInputStream input;
        BufferedOutputStream output;
        int status;
        byte[] buffer = new byte[1024];
                
        try {
            // costruzione dello URL di interrogazione del web-service
            url = prefix + "center=" + Double.toString(latitude) + "," + Double.toString(longitude);
            url = url + "&size=" + Integer.toString(width) + "x" + Integer.toString(height);
            url = url + "&zoom=" + Integer.toString(zoom);
            server = new URL(url);
            service = (HttpURLConnection)server.openConnection();
            // impostazione header richiesta
            service.setRequestProperty("Host", "maps.googleapis.com");
            service.setRequestProperty("Accept", "image/png");
            // impostazione metodo di richiesta GET
            service.setRequestMethod("GET");
            // attivazione ricezione
            service.setDoInput(true);
            // connessione al web-service
            service.connect();
            // verifica stato risposta
            status = service.getResponseCode();
            if (status != 200) {
                return; // non OK
            }
            // aperture stream di ricezione da risorsa web
            input = new BufferedInputStream(service.getInputStream());
            // aperture stream per scrittura su file
            output = new BufferedOutputStream(new FileOutputStream(filename));
            // ciclo di lettura da web e scrittura su file
            int n;
            while ((n=input.read(buffer, 0, buffer.length)) != -1) {
                output.write(buffer, 0, n);
            }
            input.close();
            output.close();
            saved = true;
        }
        catch (IOException e) {
        }
    }

    public Map(String address, int zoom, int width, int height, String filename) {
        URL server;
        HttpURLConnection service;
        BufferedInputStream input;
        BufferedOutputStream output;
        int status;
        byte[] buffer = new byte[1024];
        Geocoding geocoding = new Geocoding(address, "tmp.xml");
                
        try {
            // costruzione dello URL di interrogazione del web-service
            url = prefix + "center=" + Double.toString(geocoding.getLatitude()) + "," + Double.toString(geocoding.getLongitude());
            url = url + "&size=" + Integer.toString(width) + "x" + Integer.toString(height);
            url = url + "&zoom=" + Integer.toString(zoom);
            server = new URL(url);
            service = (HttpURLConnection)server.openConnection();
            // impostazione header richiesta
            service.setRequestProperty("Host", "maps.googleapis.com");
            service.setRequestProperty("Accept", "image/png");
            // impostazione metodo di richiesta GET
            service.setRequestMethod("GET");
            // attivazione ricezione
            service.setDoInput(true);
            // connessione al web-service
            service.connect();
            // verifica stato risposta
            status = service.getResponseCode();
            if (status != 200) {
                return; // non OK
            }
            // aperture stream di ricezione da risorsa web
            input = new BufferedInputStream(service.getInputStream());
            // aperture stream per scrittura su file
            output = new BufferedOutputStream(new FileOutputStream(filename));
            // ciclo di lettura da web e scrittura su file
            int n;
            while ((n=input.read(buffer, 0, buffer.length)) != -1) {
                output.write(buffer, 0, n);
            }
            input.close();
            output.close();
            saved = true;
        }
        catch (IOException e) {
        }
        catch (GeocodingException e) {
        }
    }
    
    
    public boolean isSaved() {
        return saved;
    }

    public static void main(String args[]) {
        /*
        Map home = new Map(43.499056, 10.323778, 15, 500, 400, "map.png");
        if (home.isSaved())
            System.out.println("Mappa salvata correttamente.");
        else
            System.out.println("Errore recupero mappa!");
        */
        Map school = new Map("Via Galilei 68 Livorno", 15, 500, 400, "map.png");
        if (school.isSaved())
            System.out.println("Mappa salvata correttamente.");
        else
            System.out.println("Errore recupero mappa!");
    }
}
