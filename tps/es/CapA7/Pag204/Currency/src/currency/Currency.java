/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package currency;

/**
 *
 * @author flomb
 */
import java.io.*;
import java.net.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

class CurrencyException extends Exception {
}

public class Currency {
 //private String prefix = "http://www.ecb.eu/exchange?currency=";   non è più disponibile
// private String prefix = "https://api.exchangeratesapi.io/latest?symbols=";  ritorna un formato non XML
 private String prefix = "https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml"; //OK anche se non usa GET
 private String url;
 private boolean saved = false;
 private boolean parsed = false;
 private double rate;

    
 public Currency() throws CurrencyException {
  URL server;
  HttpURLConnection service;
  BufferedReader input;
  BufferedWriter output;
  String line;
  int status;
  
  
  try {
   url = prefix ;//+ URLEncoder.encode(currency, "UTF-8"); // costruzione dello URL di interrogazione del servizio
   server = new URL(url);
   service = (HttpURLConnection)server.openConnection();
   //service.setRequestProperty("Host", "www.ecb.eu"); // impostazione header richiesta: host interrogato
   service.setRequestProperty("Host", "www.ecb.europa.eu"); // impostazione header richiesta: host interrogato
   service.setRequestProperty("Accept", "application/xml"); // impostazione header richiesta: formato risposta (XML)
   service.setRequestProperty("Accept-Charset", "UTF-8"); // impostazione header richiesta: codifica risposta (UTF-8)
   service.setRequestMethod("GET"); // impostazione metodo di richiesta GET
   service.setDoInput(true); // attivazione ricezione
   service.connect(); // connessione al servizio
   status = service.getResponseCode(); // verifica stato risposta
   if (status != 200) {
    return; // errore
   }
   // apertura stream di ricezione da risorsa web 
   input = new BufferedReader(new InputStreamReader(service.getInputStream(), "UTF-8"));
   // apertura stream per scrittura su file
   output = new BufferedWriter(new FileWriter("rate.xml"));
   // ciclo di lettura da web e scrittura su file
   while ((line = input.readLine()) != null) {
    output.write(line); output.newLine();
   }
   input.close(); output.close();
   saved = true;   // ha correttamente generato il nuovo file XML con i valori odierni
  }
  catch (IOException e) {}
 }
 
 private void parseXML(String currency) throws CurrencyException {
        if (!saved) {
            throw new CurrencyException();
        }
        try {
            
            // Controlla che currency appartenga ai sei tipi accettati
        if (!currency.equalsIgnoreCase("USD") &&
            !currency.equalsIgnoreCase("JPY") &&
            !currency.equalsIgnoreCase("GBP") &&
            !currency.equalsIgnoreCase("CHF") &&
            !currency.equalsIgnoreCase("RUB") &&
            !currency.equalsIgnoreCase("CNY"))
                throw new CurrencyException();
            
            
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse("rate.xml");
            Element root = document.getDocumentElement();
            
            String s1=root.getNodeName();
            
           
           // ottengo il nodo radice DOM
           Node myNode = root.getFirstChild();
           s1=myNode.getNodeName();
           // Navigo tra figli e fratelli fino al secondo nodo Cube
           myNode = myNode.getNextSibling();
           s1=myNode.getNodeName();
           myNode = myNode.getNextSibling();
           s1=myNode.getNodeName();
           myNode = myNode.getNextSibling();
           s1=myNode.getNodeName();
           myNode = myNode.getNextSibling();
           s1=myNode.getNodeName();
           myNode = myNode.getNextSibling();
           s1=myNode.getNodeName();
           myNode = myNode.getFirstChild();
           s1=myNode.getNodeName();
           myNode = myNode.getNextSibling();
           s1=myNode.getNodeName();
           myNode = myNode.getFirstChild();
           s1=myNode.getNodeName();
           myNode = myNode.getNextSibling();
           s1=myNode.getNodeName();
           Element e=(Element)myNode;
           // Verifico di esssere sul primo nodo, USD
          
           String sGetCurrency=e.getAttribute("currency");
           String sFirstCurrency="USD";
           
           // Controlla che la prima valuta della lista sia USD
           if(!sGetCurrency.equals(sFirstCurrency))
                throw new CurrencyException();
           // Naviga al nodo delal valuta richiesta
           while(!sGetCurrency.equals(currency)){
                myNode = myNode.getNextSibling();
                s1=myNode.getNodeName();
                myNode = myNode.getNextSibling();
                s1=myNode.getNodeName();
                sGetCurrency=((Element)myNode).getAttribute("currency");
                s1=myNode.getNodeName();
           }
           
           rate= Float.parseFloat(((Element)myNode).getAttribute("rate"));
           
          /* NodeList list =myNode.getChildNodes();
          
          
          
            
          if (list != null && list.getLength() > 0) {
              //rate = Float.parseFloat(list.item(0).getFirstChild().getNodeValue());
              s1=list.item(0).getFirstChild().getNodeValue();
              parsed = true;
         
}*/
        }
        catch (IOException e) {
            throw new CurrencyException();
        }
        catch (ParserConfigurationException e) {
            throw new CurrencyException();
        }
        catch (SAXException e) {
            throw new CurrencyException();
        }
    }
  
  public double getImport(float euro,String currency) throws CurrencyException {
        //if (!parsed) {
        //    throw new CurrencyException();
        //}
        if (!parsed)
            parseXML(currency);
        return euro*rate;
    }
  
    public static void main(String args[]) {
        try {
            Currency currency = new Currency();
            double dChange = currency.getImport(Float.parseFloat(args[0]),args[1]);
            System.out.print(args[0]+" € = ");
            System.out.format("%.2f ",dChange);
            System.out.println(args[1] + " al cambio odierno");
        }
        catch (CurrencyException e) {
            System.err.println("Errore interrogazione servizio!");
        }
    }
}
