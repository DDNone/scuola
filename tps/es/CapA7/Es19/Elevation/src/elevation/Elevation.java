/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package elevation;

/**
 *
 * @author flomb
 */
import java.io.*;
import java.net.*;
import javax.net.ssl.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;


class ElevationException extends Exception {
}

public class Elevation {
    private String url = "https://maps.googleapis.com/maps/api/elevation/xml?key=AIzaSyDSSOWTvCtX1EmtDMunUfNqRJSbxzI3Tsk&locations=";
    private boolean saved = false;
    private boolean parsed = false;
    private double[] elevation; 
    
    public Elevation(String filename) throws IOException {
        URL server;
        HttpsURLConnection service;
        BufferedReader input;
        BufferedWriter output;
        int status;
        String line;
        String[] section;
        String[] token;
        TextFile file = new TextFile(filename, 'R');
        int count = 0;
        double latitude;
        double longitude;
        
        try {
            line = file.fromFile();
            section = line.split(";");
            if (section.length >= 2) {
                token = section[0].split(",");
                if (token.length >= 3) {
                    latitude = Double.parseDouble(token[0]) + Double.parseDouble(token[1])/60. + Double.parseDouble(token[2])/3600.;
                    token = section[1].split(",");
                    if (token.length >= 3) {
                        longitude = Double.parseDouble(token[0]) + Double.parseDouble(token[1])/60. + Double.parseDouble(token[2])/3600.;
                        url = url + latitude + "," + longitude; 
                        count++;
                    }
                }
            }
            while (true) {
                line = file.fromFile();
                section = line.split(";");
                if (section.length >= 2) {
                    token = section[0].split(",");
                    if (token.length >= 3) {
                        latitude = Double.parseDouble(token[0]) + Double.parseDouble(token[1])/60. + Double.parseDouble(token[2])/3600.;
                        token = section[1].split(",");
                        if (token.length >= 3) {
                            longitude = Double.parseDouble(token[0]) + Double.parseDouble(token[1])/60. + Double.parseDouble(token[2])/3600.;
                            url = url + "|" + latitude + "," + longitude; 
                            count++;
                        }
                    }
                }
            }
        }
        catch (FileException exception) {
        }
        catch(IOException exception) {
        }
        finally {
            file.closeFile();
        }
        elevation = new double[count];
        server = new URL(url);
        service = (HttpsURLConnection)server.openConnection();
        // impostazione header richiesta
        service.setRequestProperty("Host", "maps.googleapis.com");
        service.setRequestProperty("Accept", "application/xml");
        service.setRequestProperty("Accept-Charset", "UTF-8");
        // impostazione metodo di richiesta GET
        service.setRequestMethod("GET");
        // attivazione ricezione
        service.setDoInput(true);
        // connessione al web-service
        service.connect();
        // verifica stato risposta
        status = service.getResponseCode();
        if (status != 200) {
            return; // non OK
        }
        // aperture stream di ricezione da risorsa web
        input = new BufferedReader(new InputStreamReader(service.getInputStream(), "UTF-8"));
        // aperture stream per scrittura su file
        output = new BufferedWriter(new FileWriter("answer.xml"));
        // ciclo di lettura da web e scrittura su file
        while ((line = input.readLine()) != null) {
            output.write(line);
            output.newLine();
        }
        input.close();
        output.close();
        saved = true;
    }

    private void parseXML() throws ElevationException {
        if (!saved) {
            throw new ElevationException();
        }
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            // costruzione albero DOM del file XML
            Document document = builder.parse("answer.xml");
            // estrazione elemento radice
            Element root = document.getDocumentElement();
            // ricerca elemento "status"
            NodeList list = root.getElementsByTagName("status");
            if (list != null && list.getLength() > 0) {
                // verifica esito (valore elemento "status" = "OK")
                if (list.item(0).getFirstChild().getNodeValue(). equalsIgnoreCase("OK")) {
                    // ricerca elementi "elevation"
                    list = root.getElementsByTagName("elevation");
                    if (list != null && list.getLength() > 0) {
                        for (int i=0; i<list.getLength(); i++) {
                            Element elev = (Element)list.item(i);
                            // conversione in valore numerico
                            elevation[i] = Double.parseDouble(elev.getFirstChild().getNodeValue());
                        }
                        parsed = true;
                    }
                }
            }
        }
        catch (IOException exception) {
            throw new ElevationException();
        }
        catch (ParserConfigurationException exception) {
            throw new ElevationException();
        }
        catch (SAXException exception) {
            throw new ElevationException();
        }
    }

    public double[] getElevation() throws ElevationException {
        if (!saved) {
            throw new ElevationException();
        }
        if (!parsed) {
            parseXML();
        }
        return elevation;
    }
    
    public void saveToFile(String filename) throws ElevationException, FileException, IOException {
        TextFile file = new TextFile(filename, 'W');
        double[] elevation = getElevation();
        
        for (int i=0; i<elevation.length; i++) {
            file.toFile(Double.toString(elevation[i]));
        }
        file.closeFile();
    }

    public static void main(String args[]) {
        try {
            Elevation elevation = new Elevation("path.csv");
            double[] path_elevation = elevation.getElevation();
            for (int i=0; i<path_elevation.length; i++) {
                System.out.println(path_elevation[i]+"m");
            }
            elevation.saveToFile("elevation.csv");
        }
        catch (IOException e) {
            System.err.println("Errore gestione file!");
        }
        catch (FileException e) {
            System.err.println("Errore gestione file!");
        }
        catch (ElevationException e) {
            System.out.println("Errore invocazione web-service!");
        }
    }
}
