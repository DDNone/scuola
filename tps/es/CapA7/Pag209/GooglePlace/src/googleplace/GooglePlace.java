/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package googleplace;

/**
 *
 * @author flomb
 */
import java.io.*;
import java.net.*;
import javax.net.ssl.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

    public class GooglePlace { 
        private String add = "https://maps.googleapis.com/maps/api/place/add/xml?key=AIzaSyDSSOWTvCtX1EmtDMunUfNqRJSbxzI3Tsk";
        private String del = "https://maps.googleapis.com/maps/api/place/delete/xml?key=AIzaSyDSSOWTvCtX1EmtDMunUfNqRJSbxzI3Tsk";
    
    public String addPlace(String report_filename) { 
        String reference = "";
        String line; 
        File report_file = new File(report_filename); 
        Long n = report_file.length(); // numero di byte del report
        try {   URL server = new URL(add); 
                HttpsURLConnection service = (HttpsURLConnection) server.openConnection(); // impostazione header richiesta 
                service.setRequestProperty("Host", "maps.googleapis.com");
                service.setRequestProperty("Content-type", "application/xml"); 
                service.setRequestProperty("Content-length", n.toString()); 
                service.setRequestProperty("Accept-Charset", "UTF-8"); 
                service.setRequestProperty("Accept", "application/xml"); // attivazione ricezione e trasmissione 
                service.setDoOutput(true); 
                service.setDoInput(true);
                // impostazione metodo di richiesta POST 
                service.setRequestMethod("POST"); 
                // apertura stream di lettura file di report 
                BufferedReader report = new BufferedReader(new FileReader(report_filename)); 
                // apertura stream di invio dati a web-service 
                BufferedWriter output = new BufferedWriter(new OutputStreamWriter(service.getOutputStream(), "UTF-8")); 
                // ciclo di lettura body da file e invio a web-service 
                while ((line = report.readLine()) != null) { 
                    output.write(line); 
                      output.newLine();
                }
                 // connessione al web-service 
                service.connect(); 
                output.flush(); 
                output.close(); 
                report.close();
                // verifica stato risposta 
                int status = service.getResponseCode(); 
                if (status != 200 && status != 201) { 
                  // non OK 
                  return "";
                }
                // apertura stream di ricezione dati da web-service 
                BufferedReader input = new BufferedReader(new InputStreamReader(service.getInputStream(), "UTF-8")); 
                // apertura stream di scrittura su file 
                BufferedWriter file = new BufferedWriter(new FileWriter("response.xml")); 
                // ciclo di ricezione da web-service e scrittura su file 
                while ((line = input.readLine()) != null) { 
                    file.write(line); 
                    file.newLine();
                 }
                input.close(); 
                file.flush(); 
                file.close();
                // costruzione albero DOM del file XML 
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance(); 
                DocumentBuilder builder = factory.newDocumentBuilder(); 
                Document document = builder.parse("response.xml"); 
                // estrazione elemento radice 
                Element root = document.getDocumentElement(); 
                // ricerca elemento "status" 
                NodeList list = root.getElementsByTagName("status"); 
                if (list != null && list.getLength() > 0) { 
                // verifica stato risposta ("OK") 
                if (list.item(0).getFirstChild().getNodeValue().equalsIgnoreCase("OK")) { 
                        // ricerca elemento "place_id"
                    list = root.getElementsByTagName("place_id"); 
                if ( list != null && list.getLength() > 0) {
                reference = list.item(0).getFirstChild().getNodeValue(); 
                } 
            } 
        } 
     }
     catch (IOException exception) { }
     catch (ParserConfigurationException exception) { }
     catch (SAXException exception) { }
     return reference; 
}
public String delPlace(String reference) { Integer n;
    String body = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"; 
    String line;
    try {
        // costruzione body richiesta 
        body += "<PlaceDeleteRequest>\r\n"; 
        body += "<place_id>"; 
        body += reference; 
        body += "</place_id>\r\n"; 
        body += "</PlaceDeleteRequest>\r\n"; 
        n = body.length(); 
        URL server = new URL(del); 
        HttpsURLConnection service = (HttpsURLConnection) server.openConnection(); 
        // impostazione header richiesta 
        service.setRequestProperty("Host", "maps.googleapis.com"); 
        service.setRequestProperty("Content-type", "application/xml"); 
        service.setRequestProperty("Content-length", n.toString()); 
        service.setRequestProperty("Accept-Charset", "UTF-8"); 
        service.setRequestProperty("Accept", "application/xml"); 
        // attivazione ricezione e trasmissione 
        service.setDoOutput(true); 
        service.setDoInput(true);
        // impostazione metodo di richiesta POST 
        service.setRequestMethod("POST"); 
        // apertura stream di invio dati a web-service 
        BufferedWriter output = new BufferedWriter(new OutputStreamWriter(service.getOutputStream(), "UTF-8")); 
        // invio body al web-service 
        output.write(body); 
        output.flush(); 
        output.close();
        // connessione al web-service 
        service.connect(); 
        // verifica stato risposta 
        int status = service.getResponseCode(); 
        if (status != 200 && status != 201) { 
            // non OK
            return "ERROR"; 
        }
        // apertura stream di ricezione dati da web-service 
        BufferedReader input = new BufferedReader(new InputStreamReader(service.getInputStream(), "UTF-8"));
        // apertura stream di scrittura su file 
        BufferedWriter file = new BufferedWriter(new FileWriter("response.xml")); 
        // ciclo di ricezione da web-service e scrittura su file 
        while ((line = input.readLine()) != null) { 
               file.write(line); 
               file.newLine();
}
        input.close(); file.flush(); file.close();
        // costruzione albero DOM del file XML 
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance(); 
        DocumentBuilder builder = factory.newDocumentBuilder(); 
        Document document = builder.parse("response.xml"); 
        Element root = document.getDocumentElement(); 
        // ricerca elemento "status" 
        NodeList list = root.getElementsByTagName("status"); 
        if (list != null && list.getLength() > 0) { 
            return list.item(0).getFirstChild().getNodeValue();
        } else{ 
            return "ERROR"; 
        } 
    }
    catch (IOException exception) { }
    catch (ParserConfigurationException exception) { }
    catch (SAXException exception) { }
    return "ERROR"; }

public static void main(String args[]) { 
    String reference, response; 
    GooglePlace place = new GooglePlace();
    reference = place.addPlace("report.xml"); 
    if (reference.isEmpty())
        System.err.println("Errore aggiunta punto di interesse."); 
    else {
        System.out.println("ID punto di interesse: " + reference + "."); 
        response = place.delPlace(reference); 
        System.out.println("Esito eliminazione: " + response + ".");
    } 
} 
}
