
import java.io.*;
import java.net.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

class CurrencyException extends Exception {
}

public class Currency {
 private String prefix = "http://www.ecb.eu/exchange?currency=";
 private String url;
 private boolean saved = false;
 private boolean parsed = false;
 private double rate;

    
 public Currency(String currency) throws CurrencyException {
  URL server;
  HttpURLConnection service;
  BufferedReader input;
  BufferedWriter output;
  String line;
  int status;
  
  if (!currency.equalsIgnoreCase("USD") &&
      !currency.equalsIgnoreCase("JPY") &&
      !currency.equalsIgnoreCase("GBP") &&
      !currency.equalsIgnoreCase("CHF") &&
      !currency.equalsIgnoreCase("RUB") &&
      !currency.equalsIgnoreCase("CNY"))
    throw new CurrencyException();
  
  try {
   url = prefix + URLEncoder.encode(currency, "UTF-8"); // costruzione dello URL di interrogazione del servizio
   server = new URL(url);
   service = (HttpURLConnection)server.openConnection();
   service.setRequestProperty("Host", "www.ecb.eu"); // impostazione header richiesta: host interrogato
   service.setRequestProperty("Accept", "application/xml"); // impostazione header richiesta: formato risposta (XML)
   service.setRequestProperty("Accept-Charset", "UTF-8"); // impostazione header richiesta: codifica risposta (UTF-8)
   service.setRequestMethod("GET"); // impostazione metodo di richiesta GET
   service.setDoInput(true); // attivazione ricezione
   service.connect(); // connessione al servizio
   status = service.getResponseCode(); // verifica stato risposta
   if (status != 200) {
    return; // errore
   }
   // apertura stream di ricezione da risorsa web 
   input = new BufferedReader(new InputStreamReader(service.getInputStream(), "UTF-8"));
   // apertura stream per scrittura su file
   output = new BufferedWriter(new FileWriter("rate.xml"));
   // ciclo di lettura da web e scrittura su file
   while ((line = input.readLine()) != null) {
    output.write(line); output.newLine();
   }
   input.close(); output.close();
   saved = true;
  }
  catch (IOException e) {}
 }
 
 private void parseXML() throws CurrencyException {
        if (!saved) {
            throw new CurrencyException();
        }
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse("rate.xml");
            Element root = document.getDocumentElement();
            NodeList list = root.getElementsByTagName("rate");
            if (list != null && list.getLength() > 0) {
              rate = Float.parseFloat(list.item(0).getFirstChild().getNodeValue());
              parsed = true;
            }
        }
        catch (IOException e) {
            throw new CurrencyException();
        }
        catch (ParserConfigurationException e) {
            throw new CurrencyException();
        }
        catch (SAXException e) {
            throw new CurrencyException();
        }
    }
  
  public double getImport(float euro) throws CurrencyException {
        if (!parsed) {
            throw new CurrencyException();
        }
        if (!parsed)
            parseXML();
        return euro*rate;
    }
  
    public static void main(String args[]) {
        try {
            Currency dollaro = new Currency("USD");
            System.out.println(dollaro.getImport(1));
        }
        catch (CurrencyException e) {
            System.err.println("Errore interrogazione servizio!");
        }
    }
}
