
package tcpserver;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TCPServer {

    public static void main(String[] args) {
        try {
            TdServer ts= new TdServer(9090);
            ts.start();
            int c=System.in.read();
            ts.interrupt();
            ts.join();
        } catch (IOException ex) {
            Logger.getLogger(TCPServer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(TCPServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
