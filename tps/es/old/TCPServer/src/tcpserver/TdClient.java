package tcpserver;

import java.io.*;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TdClient extends Thread {

    Socket connection;
    InputStream in;
    OutputStream out;

    public TdClient(Socket connection) throws IOException {
        this.connection = connection;
        in = connection.getInputStream();
        out = connection.getOutputStream();
    }

    @Override
    public void run() {
        int n;
        byte[] buffer = new byte[1024];

        //ricezione dati dal client
        try {
            while ((n = in.read()) != -1) {
                if (n > 0) {
                    //implementazione protocollo
                    System.out.println("buffer: " + buffer);
                    out.write(buffer, 0, n);
                    out.flush();
                }
            }
        } catch (Exception e) {
            try {
                //e.printStackTrace();
                System.out.println("close connection "+this.getClass());
                in.close();
                out.close();
                connection.shutdownInput();
                connection.shutdownOutput();
                this.connection.close();
                
            } catch (IOException ex) {
                Logger.getLogger(TdClient.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

}
