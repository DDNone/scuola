package tcpserver;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TdServer extends Thread{
    int port;
    ServerSocket server;

    public TdServer(int port) throws IOException {
        this.port = port;
        this.server = new ServerSocket(port);
        server.setSoTimeout(10000);
    }

    @Override
    public void run() {
        Socket cn=null;
        
        while(!Thread.interrupted()){
            try {
                cn=server.accept();
                System.out.println("connessione richiesta da: "+cn.getInetAddress().toString()+" port: "+cn.getPort());
                Thread client= new TdClient(cn);
                client.start();
            } catch (IOException ex) {
                Logger.getLogger(TdServer.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        
        try {
            server.close();
        } catch (IOException ex) {
            Logger.getLogger(TdServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    
}
