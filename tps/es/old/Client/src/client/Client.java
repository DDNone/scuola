package client;
import java.io.*;
import java.net.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Date;

import javax.swing.JOptionPane;
class Client {
 public static void main(String[] args) throws IOException {
     for(int i=0;i<5;i++){
        String serverAddress = JOptionPane.showInputDialog(
            "Enter IP Address of a machine that is\n" +
            "running the date service on port 9090:");
        
        Socket s = new Socket(serverAddress, 9090);
        
        //creazione protocollo
        PrintWriter out =new PrintWriter(s.getOutputStream(), true);
        out.println("barzigola".getBytes());
        //lettura
        BufferedReader input =new BufferedReader(new InputStreamReader(s.getInputStream()));
        String answer = input.readLine();
        
        
        //output
        JOptionPane.showMessageDialog(null, answer);
    }
 }
}