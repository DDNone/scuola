
package server;

import java.io.BufferedReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.SocketTimeoutException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MultiThreadedServer extends Thread {

    protected int serverPort = 8080;
    protected ServerSocket serverSocket = null;
    protected boolean isStopped = false;
    protected Thread runningThread = null;

    public MultiThreadedServer(int port) {
        this.serverPort = port;
        try {
            this.serverSocket = new ServerSocket(this.serverPort);
            serverSocket.setSoTimeout(10000);//timeout 
        } catch (IOException e) {
            throw new RuntimeException("Cannot open port ", e);
        }
    }

    public void run() {
        while (!Thread.interrupted()) {
            Socket clientSocket = null;
            try {
                clientSocket = this.serverSocket.accept();
                System.out.println();
                //definizione protocollo gestito in client thread
                Thread client = new ClientThread(clientSocket);
                client.start();
            } catch (IOException e) {
                throw new RuntimeException("Error accepting client connection", e);
            } catch (Exception ex) {
                throw new RuntimeException("Connection timeout",ex);
                
            } finally {//chiusura soket client
                if (clientSocket != null) {
                    try {
                        clientSocket.shutdownOutput();
                        clientSocket.close();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            }

        }
        //chiusura server
        try {
            System.out.println("Server Stopped.");
            serverSocket.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
