package server;

import java.io.*;
import java.net.*;

import java.util.Date;
import javax.swing.JOptionPane;

class Server {

   static  int c;

    public static void main(String argv[]) throws Exception {
        MultiThreadedServer server = new MultiThreadedServer(9090);
        try {
            
            server.start();
            c = System.in.read();
            server.interrupt();
            server.join();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Stopping Server");
        //server.stop();
    }
}
