package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.*;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClientThread extends Thread {

    public Socket connection;
    PrintWriter out;
    BufferedReader input;

    public ClientThread(Socket connection) {
        try {
            this.connection = connection;
            out = new PrintWriter(connection.getOutputStream(), true);
            input = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        } catch (IOException ex) {
            Logger.getLogger(ClientThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void run() {
        try {
            String answer = input.readLine();
            //protocollo
            if (answer.equals("barzigola")) {
                    System.out.println("answer: " + answer);
                    //implementazione programma
                    try {
                        PrintWriter out = new PrintWriter(connection.getOutputStream(), true);
                        out.println(new Date().toString());
                    } catch(Exception e){
                        e.printStackTrace();
                    }
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
