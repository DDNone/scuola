package server;

import java.io.*;
import java.net.*;

import java.util.Date;

class Server {

    public static void main(String argv[]) throws Exception {
        ServerSocket server = new ServerSocket(9090); 
        Rubrica rubrica = new Rubrica();
        
        for (int i = 0; i < 10; i++)
        {
            rubrica.addContact(new Persona(i+"", i+10+""));
        }
        
        System.out.println("Servizio attivo...");
        while (!Thread.interrupted())
        {
            Socket socket = server.accept();
            ThreadManage client = new ThreadManage(socket,rubrica); 
            client.start();
        }
        server.close();
    }
}
