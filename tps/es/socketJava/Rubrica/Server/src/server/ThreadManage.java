package server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ThreadManage extends Thread {

    Rubrica rubrica;
    Socket socket;

    ThreadManage(Socket socket, Rubrica rubrica) {
        System.out.println("Creazione thread...");
        this.socket = socket;
        this.rubrica = rubrica;
    }

    @Override
    public void run() {
        System.out.println("Esecuzione thread...");
        int n;
        char[] buffer = new char[1024];
        try {
            BufferedReader streamKeyboard = new BufferedReader(new InputStreamReader(System.in));
            BufferedReader streamSocketIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            BufferedWriter streamSocketOut = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

            while ((n = streamSocketIn.read(buffer)) != -1) {
                if (n > 0) {
                    String response = new String();
                    response=String.copyValueOf(buffer);
                    //System.out.println("Response: " + response);
                    String[] values = response.split(" ");
                    String richiesta = values[0].trim();
                    //System.out.println("richiesta: "+richiesta);

                    String valore = values[1].trim();
                    System.out.println("valore: "+valore);
                    String rewrite = "";

                    try {

                        if (values.length == 3) {
                            rewrite = values[2].trim();
                        }
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();

                    }
                          

                    String sender;
                    switch (Integer.parseInt(richiesta)) {
                        case 0: /* Get Nome */ {
                            sender = rubrica.get(valore,"");
                            break;
                        }

                        case 1: {
                            sender = rubrica.get("",valore);
                            break;
                        }

                        case 2: {
                            rubrica.addContact(new Persona(valore, rewrite));
                            sender = "Contatto aggiunto...";
                            break;
                        }

                        case 3: {
                            
                            rubrica.modify("",valore,rewrite,"");
                            
                            sender = "Contatto modificato...";

                            break;
                        }

                        case 4: {
                            
                            rubrica.modify(valore,"","",rewrite);
                           
                            sender = "Contatto modificato...";
                            break;
                        }

                        default: {
                            sender = "Errore generico...";
                            break;
                        }
                    }
                    streamSocketOut.write(sender);
                    streamSocketOut.flush();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
