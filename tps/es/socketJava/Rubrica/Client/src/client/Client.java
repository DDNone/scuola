package client;

import java.io.*;
import java.net.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

import javax.swing.JOptionPane;

class Client {

    public static void main(String[] args) throws IOException {

        String serverAddress = "0.0.0.0";
        Socket socket = new Socket(serverAddress, 9090);
        BufferedReader streamKeyboard = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader streamSocketIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        BufferedWriter streamSocketOut = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        streamSocketOut.flush();

        System.out.println(""
                + "Rubrica Telefonica\n"
                + "\t1 - Ricerca numero\n"
                + "\t2 - Ricerca nominativo\n"
                + "\t3 - Aggiunta contatto\n"
                + "\t4 - Modifica nominativo\n"
                + "\t5 - Modifica numero"
        );
        switch (Integer.parseInt(streamKeyboard.readLine())) {
            case 1: {
                System.out.println("Inserire nominativo: ");
                String valore = streamKeyboard.readLine();

                streamSocketOut.write(0 + " " + valore);
                streamSocketOut.flush();
                break;
            }
            case 2: {
                System.out.println("Inserire numero: ");
                String valore = streamKeyboard.readLine();

                streamSocketOut.write(1 + " " + valore);
                streamSocketOut.flush();
                break;
            }
            case 3: {
                System.out.println("Aggiunta contatto\n\tInserisci nominativo:  ");
                String nominativo = streamKeyboard.readLine();
                System.out.println("inserisci numero:  ");
                String numero = streamKeyboard.readLine();

                streamSocketOut.write(2 + " " + nominativo + " " + numero);
                streamSocketOut.flush();
                break;
            }
            case 4: {
                System.out.println("Modifica nominativo\n\tInserisci numero: ");
                String numero = streamKeyboard.readLine();
                System.out.println("Inserisci nominativo nuovo: ");
                String nominativo = streamKeyboard.readLine();

                streamSocketOut.write(3 + " " + numero + " " + nominativo);
                streamSocketOut.flush();
                break;
            }
            case 5: {
                System.out.println("Modifica numero\n\tInserisci nominativo: ");
                String nominativo = streamKeyboard.readLine();
                System.out.println("Inserisci numero nuovo: ");
                String numero = streamKeyboard.readLine();

                streamSocketOut.write(4 + " " + nominativo + " " + numero);
                streamSocketOut.flush();
                break;
            }
            default: {
                System.err.println("Richiesta non valida...");
                System.exit(1);
            }
        }
        int n;
        char[] buffer = new char[1024];

        while ((n = streamSocketIn.read(buffer)) != -1) {
            if (n > 0) {
                String response = String.copyValueOf(buffer);
                System.out.println("Response: " + response);
            }
        }

        System.exit(0);
    }
}
