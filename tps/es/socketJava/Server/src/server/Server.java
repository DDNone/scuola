/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.io.*;
import java.net.*;

import java.util.Date;
import javax.swing.JOptionPane;

class Server {

    public static void main(String argv[]) throws Exception {
        MultiThreadedServer server = new MultiThreadedServer(9090);

        new Thread(server)
                .start();

        try {
            Thread.sleep(200 * 1000);//durata in cui resta attivo il server
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Stopping Server");
        server.stop();
    }
}
