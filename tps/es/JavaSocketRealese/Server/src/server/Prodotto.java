package server;
public class Prodotto {
    int prodotto;
    int quantita;
    String desc;

    public Prodotto(int prodotto, int quantita, String desc) {
        this.prodotto = prodotto;
        this.quantita = quantita;
        this.desc = desc;
    }

    public int getQuantita() {
        return quantita;
    }

    public void setQuantita(int quantita) {
        this.quantita = quantita;
    }

    public int getProdotto() {
        return prodotto;
    }

    public String getDesc() {
        return desc;
    }

    public void setProdotto(int prodotto) {
        this.prodotto = prodotto;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
    
}
