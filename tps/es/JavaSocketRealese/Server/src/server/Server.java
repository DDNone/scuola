package server;

import java.io.*;
import java.net.*;

import java.util.Date;

class Server {

    public static void main(String argv[]) throws Exception {
        ServerSocket server = new ServerSocket(9090);  
        Prodotti prodotti =  new Prodotti();
        
        for (int i = 0; i < 10; i++)
        {
            prodotti.add(new Prodotto(i,i,"desc"));
        }
        
        System.out.println("Servizio attivo...");
        while (!Thread.interrupted())
        {
            Socket socket = server.accept();
            ThreadManage client = new ThreadManage(socket,prodotti); 
            client.start();
        }
        server.close();
    }
}
