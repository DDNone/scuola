package server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ThreadManage extends Thread {

    Prodotti prodotti;
    Socket socket;

    ThreadManage(Socket socket, Prodotti prodotti) {
        System.out.println("Creazione thread...");
        this.socket = socket;
        this.prodotti = prodotti;
    }

    @Override
    public void run() {
        System.out.println("Esecuzione thread...");
        int n;
        char[] buffer = new char[1024];
        try {
            BufferedReader streamKeyboard = new BufferedReader(new InputStreamReader(System.in));
            BufferedReader streamSocketIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            BufferedWriter streamSocketOut = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

            while ((n = streamSocketIn.read(buffer)) != -1) {
                if (n > 0) {
                    String response = String.copyValueOf(buffer);
                    System.out.println("Response: " + response);

                    String request = "", cod = "", quantita = "";
                    int flag = 0;//numeri di spazi
                    for (int i = 0; i < buffer.length; i++) {
                        if (buffer[i] == ' ') {
                            flag++;
                            //request+=""+buffer[i];
                        } else {// 1 27
                            switch (flag) {
                                case 0: {
                                    request += "" + buffer[i];
                                    break;
                                }
                                case 1: {
                                    cod += "" + buffer[i];
                                    break;
                                }
                                case 2: {
                                    quantita += buffer[i];
                                    break;
                                }
                            }

                        }
                    }
                    System.out.println("Request " + request + "\nCodice: " + cod + "\nquantita: " + quantita + "\n");

                    if (request.equals("1")== true) {
                        cod = cod.trim();//magheggio barzigoliano
                        System.out.println("Interrogazione codice prodotto..." );
                        Prodotto tmp = new Prodotto(Integer.parseInt(cod), 0, "fottiti");
                        
                        n = 0;
                        if (prodotti.isPresent(tmp)) {
                            n = prodotti.finder(tmp);
                        }
                        
                        System.out.println("N: " + n);
                        streamSocketOut.write(n+"");
                        streamSocketOut.flush();

                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
