package server;

import java.util.ArrayList;

public class Prodotti {
    ArrayList prodotti;
    
    Prodotti()
    {
        prodotti = new ArrayList();
    }
    
    synchronized void add(Prodotto prodotto)
    {
        prodotti.add(prodotto);
    }
    
    synchronized Prodotto get(int i)
    {   
        Prodotto tmp = (Prodotto) prodotti.get(i);
        prodotti.remove(i);
        return tmp;
    }
    
    synchronized boolean isPresent(Prodotto prodotto)
    {
        for (int i = 0; i < prodotti.size(); i++)
        {
            Prodotto tmp = (Prodotto) prodotti.get(i);
            
            if (tmp.prodotto == prodotto.prodotto)
            {
                return true;
            }
        }
        return false;
    }
    
    synchronized int finder(Prodotto prodotto)
    {
        
        for (int i = 0; i < prodotti.size(); i++)
        {
            Prodotto tmp = (Prodotto) prodotti.get(i);
            
            if (tmp.prodotto == prodotto.prodotto)
            {
                return i;
            }
        }
        return -1;
    }
    
    synchronized boolean modificaQuantita(int i, int quantita)
    {
        Prodotto tmp = (Prodotto) prodotti.get(i);
        
        if (tmp.getQuantita() < quantita)
        {
            return false;
        }
        tmp.setQuantita(tmp.getQuantita()+quantita);
        prodotti.remove(i);
        prodotti.add(i, tmp);
        return true;
    }
}
