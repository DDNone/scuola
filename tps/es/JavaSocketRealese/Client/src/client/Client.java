package client;

import java.io.*;
import java.net.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

import javax.swing.JOptionPane;

class Client {

    public static void main(String[] args) throws IOException {

        String serverAddress = "0.0.0.0";
        Socket socket = new Socket(serverAddress, 9090);
        BufferedReader streamKeyboard = new BufferedReader(new InputStreamReader(System.in));//tastiera
        
        BufferedReader streamSocketIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        BufferedWriter streamSocketOut = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        streamSocketOut.flush();

        System.out.println(""
                + "Menu\n"
                + "\t1 - Interrogare\n"
                + "\t2 - Modifica\n"
        );
        switch (Integer.parseInt(streamKeyboard.readLine())) {
            case 1: {
                System.out.println("Inserisci codice prodotto: ");
                String prodotto = streamKeyboard.readLine();

                streamSocketOut.write(1 + " " + prodotto);
                streamSocketOut.flush();
                break;
            }
            case 2: {
                System.out.println("Inserisci codice prodotto: ");
                String prodotto = streamKeyboard.readLine();
                System.out.println("Inserisci quantita: ");
                String quantita = streamKeyboard.readLine();

                streamSocketOut.write(2 + " " + prodotto + " " + quantita);
                streamSocketOut.flush();
                break;
            }
            default: {
                System.err.println("Richiesta non valida...");
                System.exit(1);
            }
        }
        int n;
        char[] buffer = new char[1024];

        while ((n = streamSocketIn.read(buffer)) != -1) {
            if (n > 0) {
                String response = String.copyValueOf(buffer);
                System.out.println("Response: " + response);
            }
        }

        System.exit(0);
    }
}
