package librijaxb;

import Libri.Libri;
import Libri.TipoLibroGenere;
import com.sun.jndi.cosnaming.IiopUrl.Address;
import java.math.*;
import javax.xml.bind.*;
import java.io.*;
import javax.xml.transform.Result;

/**
 *
 * @author flavio.lombella
 */
public class LibriJAXB {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws JAXBException, IOException {
        Libri libri = new Libri();
        JAXBContext context = JAXBContext.newInstance("Libri");
        TipoLibroGenere libro1 = new TipoLibroGenere("fantascienza", "Stefano Benni", "Terra!", new BigDecimal(10.5));
        libri.addLibro(libro1);
        TipoLibroGenere libro2 = new TipoLibroGenere("romanzo", "Mark Twain",
                "Le avventure di Huckleberry Finn", new BigDecimal(9.9));
        libri.addLibro(libro2);
        TipoLibroGenere libro3 = new TipoLibroGenere("romanzo", "Herman Melville", "Moby Dick", new BigDecimal(15.0));
        libri.addLibro(libro3);
        Marshaller marshaller = context.createMarshaller();
        marshaller.marshal(libri, new FileWriter("books.xml"));

        marshaller.marshal(libri, (Result) new FileReader("books.xml"));

        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        File file = new File("input.xml");
        Address address = (Address) unmarshaller.unmarshal(file);
    }
}
