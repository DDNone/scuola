package es35;
public class Rilevazione {
    int ora;
    double temperature;
    int umidita;
    int velocita;
    int direzione;
    int pioggia;

    public Rilevazione(int ora, double temperature, int umidita, int velocita, int direzione, int pioggia) {
        this.ora = ora;
        this.temperature = temperature;
        this.umidita = umidita;
        this.velocita = velocita;
        this.direzione = direzione;
        this.pioggia = pioggia;
    }
    
}
