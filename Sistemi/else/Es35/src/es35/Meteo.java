package es35;

import java.util.ArrayList;
import javafx.scene.chart.PieChart.Data;

public class Meteo {
    int id;
    double x,y;//latitudine e longitudine
    Data data;
    
    ArrayList <Rilevazione> elRilevazioni;

    public Meteo(int id, double x, double y, Data data, ArrayList<Rilevazione> elRilevazioni) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.data = data;
        this.elRilevazioni = elRilevazioni;
    }
    
    
    
}
