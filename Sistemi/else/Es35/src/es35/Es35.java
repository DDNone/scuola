
package es35;

import java.io.IOException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import javafx.scene.chart.PieChart;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class Es35 {
    private List<Meteo> cassonetti;
    private boolean parsed;

    
    
    public List<Meteo> parseDocument(String filename) throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory factory;
        DocumentBuilder builder;
        Document document;
        Element root, element;
        NodeList nodelist;
        Meteo cassonetto;
        //System.out.println("parseDocumentc before "+filename);
        // validazione documento XML
        try {
         Validate.validate(filename, "schema.xsd");
        }
        catch (SAXException exception) {
         return null;
        }
        
        // creazione dell’albero DOM dal documento XML
        factory = DocumentBuilderFactory.newInstance();
        builder = factory.newDocumentBuilder();
        document = builder.parse(filename);
        root = document.getDocumentElement();
        // generazione della lista degli elementi "cassonetto"
        nodelist = root.getElementsByTagName("cassonetto");
        //System.out.println("parseDocumentc 4");
        if (nodelist != null && nodelist.getLength() > 0) {
            for (int i=0; i<nodelist.getLength(); i++) {
                element = (Element)nodelist.item(i);
                cassonetto = getMeteo(element);
                cassonetti.add(cassonetto);
                //System.out.println("parseDocument cassonetto: "+cassonetto.toString());
            }
        }
        parsed = true;
        return cassonetti;
    }
    
    private Meteo getMeteo(Element element) {
        Meteo cassonetto;
        
        String tipo = getTextValue(element, "tipo");
        String via = getTextValue(element, "via");
        int civico = getIntValue(element, "civico");
        double latitudine = getDoubleValue(element, "latitudine");
        double longitudine = getDoubleValue(element, "longitudine");
        String note = getTextValue(element, "note");
        XMLGregorianCalendar svuotamento = getDatetimeValue(element, "dataora_svuotamento");
        
        
        int id= getIntValue(element, "codice_identificativo");
        double x = getDoubleValue(element, "latitudine");
        double y = getDoubleValue(element, "longitudine");
        XMLGregorianCalendar data = getDatetimeValue(element, "data");
        
        cassonetto = new Meteo(id, double x, double y, data, ArrayList<Rilevazione> elRilevazioni);
        return cassonetto;
    }
    
    // restituisce il valore testuale dell’elemento figlio specificato
    private String getTextValue(Element element, String tag) {
        String value = null;
        NodeList nodelist;
        
        nodelist = element.getElementsByTagName(tag);
        if (nodelist != null && nodelist.getLength() > 0) {
            value = nodelist.item(0).getFirstChild().getNodeValue();
        }
        return value;
    }

    // restituisce il valore intero dell’elemento figlio specificato
    private int getIntValue(Element element, String tag) {
        return Integer.parseInt(getTextValue(element, tag));
    }

    // restituisce il valore numerico dell’elemento figlio specificato
    private double getDoubleValue(Element element, String tag) {
        return Double.parseDouble(getTextValue(element, tag));
    }
    
    // restituisce il valore data/ora dell’elemento figlio specificato
    private XMLGregorianCalendar getDatetimeValue(Element element, String tag) {
        String tmp;
        XMLGregorianCalendar value = null;
            
        NodeList nodelist;
        nodelist = element.getElementsByTagName(tag);
        if (nodelist != null && nodelist.getLength() > 0) {
            tmp = nodelist.item(0).getFirstChild().getNodeValue();
            try {
             value = DatatypeFactory.newInstance().newXMLGregorianCalendar(tmp);
            }
            catch (DatatypeConfigurationException exception) {
            }
        }
     return value;
    }
    /*
    public List<Meteo> daSvuotare() {
        ArrayList<Meteo> risultato = new ArrayList<>();
        XMLGregorianCalendar now;
        Duration H24;
        
        if (!parsed)
          return null;
        
        try {
         H24 = DatatypeFactory.newInstance().newDuration(-24*3600*1000);
         GregorianCalendar tmp = new GregorianCalendar();
         now = DatatypeFactory.newInstance().newXMLGregorianCalendar(tmp);
         now.add(H24);
        }
        catch (DatatypeConfigurationException exception) {
            return null;
        }
        
        Iterator iterator = cassonetti.iterator();
        while (iterator.hasNext()) {
             Meteo tmp = (Meteo)iterator.next();
             if (tmp.getTipo().equalsIgnoreCase("generico")) {
                 if (now.compare(tmp.getSvuotamento()) == DatatypeConstants.GREATER)
                     risultato.add(tmp);
             }
                 
        }
        return risultato;
    }
    */
    //prende tutti i Meteo
    public List<Meteo> getAll() {
        ArrayList<Meteo> risultato = new ArrayList<>();
        XMLGregorianCalendar now;
        Duration H24;
        
        if (!parsed)
          return null;
        
        try {
         H24 = DatatypeFactory.newInstance().newDuration(-24*3600*1000);
         GregorianCalendar tmp = new GregorianCalendar();
         now = DatatypeFactory.newInstance().newXMLGregorianCalendar(tmp);
         now.add(H24);
        }
        catch (DatatypeConfigurationException exception) {
            return null;
        }
        
        Iterator iterator = cassonetti.iterator();
        while (iterator.hasNext()) {
            Meteo tmp = (Meteo)iterator.next();
            risultato.add(tmp);                 
        }
        return risultato;
    }
    
    public static void main(String[] args) throws DatatypeConfigurationException {
        List<Meteo> cassonetti = null;
        Meteo tmpC=null;
        Es35 app = new Es35();
        try {
           
            app.parseDocument("documento.xml");
            //cassonetti = app.daSvuotare();
            cassonetti = app.getAll(); //prelevo tutti i cassonetti
            
            System.out.println("Numero di cassonetti da svuotare: " + cassonetti.size());
            Iterator iterator = cassonetti.iterator();
            while (iterator.hasNext()) {
                 System.out.println(iterator.next().toString());
            }
        }
        catch (ParserConfigurationException | SAXException | IOException exception) {
            System.out.println("Errore!");
        }
        //modifica
        
        /*
        GregorianCalendar c= new GregorianCalendar();
        c.setTime(new Date());
        try{
            XMLGregorianCalendar data2=DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
            tmpC= new Meteo("barzigola", "della banana", 57, 22.22, 22.22, "guarda di worcare", data2, data2);
            
        }catch(Exception e){
            e.printStackTrace();
        }
        
        //creazione file xml 
        cassonetti.add(tmpC);
        
        try {
            JAXBContext context= JAXBContext.newInstance(Meteo.class);
            Marshaller marshaller = context.createMarshaller(); 
            marshaller.marshal(cassonetti,new FileWriter("documento.xml"));
        } catch (JAXBException ex) {
            Logger.getLogger(Applicazione.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Applicazione.class.getName()).log(Level.SEVERE, null, ex);
        }
        */
    }
    
}
