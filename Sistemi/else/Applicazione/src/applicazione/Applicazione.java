package applicazione;


import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.*;
import javax.xml.datatype.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

public class Applicazione {
    private List<Cassonetto> cassonetti;
    private boolean parsed;

    public Applicazione() {
        cassonetti = new ArrayList<>();
        parsed = false;
    }
    
    public List<Cassonetto> parseDocument(String filename) throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory factory;
        DocumentBuilder builder;
        Document document;
        Element root, element;
        NodeList nodelist;
        Cassonetto cassonetto;
        //System.out.println("parseDocumentc before "+filename);
        // validazione documento XML
        try {
         Validate.validate(filename, "schema.xsd");
        }
        catch (SAXException exception) {
         return null;
        }
        
        // creazione dell’albero DOM dal documento XML
        factory = DocumentBuilderFactory.newInstance();
        builder = factory.newDocumentBuilder();
        document = builder.parse(filename);
        root = document.getDocumentElement();
        // generazione della lista degli elementi "cassonetto"
        nodelist = root.getElementsByTagName("cassonetto");
        //System.out.println("parseDocumentc 4");
        if (nodelist != null && nodelist.getLength() > 0) {
            for (int i=0; i<nodelist.getLength(); i++) {
                element = (Element)nodelist.item(i);
                cassonetto = getCassonetto(element);
                cassonetti.add(cassonetto);
                //System.out.println("parseDocument cassonetto: "+cassonetto.toString());
            }
        }
        parsed = true;
        return cassonetti;
    }
    
    private Cassonetto getCassonetto(Element element) {
        Cassonetto cassonetto;
        
        String tipo = getTextValue(element, "tipo");
        String via = getTextValue(element, "via");
        int civico = getIntValue(element, "civico");
        double latitudine = getDoubleValue(element, "latitudine");
        double longitudine = getDoubleValue(element, "longitudine");
        String note = getTextValue(element, "note");
        XMLGregorianCalendar posizionamento = getDatetimeValue(element, "data_posizionamento");
        XMLGregorianCalendar svuotamento = getDatetimeValue(element, "dataora_svuotamento");
        cassonetto = new Cassonetto(tipo, via, civico, latitudine, longitudine, note, posizionamento, svuotamento);
        return cassonetto;
    }
    
    // restituisce il valore testuale dell’elemento figlio specificato
    private String getTextValue(Element element, String tag) {
        String value = null;
        NodeList nodelist;
        
        nodelist = element.getElementsByTagName(tag);
        if (nodelist != null && nodelist.getLength() > 0) {
            value = nodelist.item(0).getFirstChild().getNodeValue();
        }
        return value;
    }

    // restituisce il valore intero dell’elemento figlio specificato
    private int getIntValue(Element element, String tag) {
        return Integer.parseInt(getTextValue(element, tag));
    }

    // restituisce il valore numerico dell’elemento figlio specificato
    private double getDoubleValue(Element element, String tag) {
        return Double.parseDouble(getTextValue(element, tag));
    }
    
    // restituisce il valore data/ora dell’elemento figlio specificato
    private XMLGregorianCalendar getDatetimeValue(Element element, String tag) {
        String tmp;
        XMLGregorianCalendar value = null;
            
        NodeList nodelist;
        nodelist = element.getElementsByTagName(tag);
        if (nodelist != null && nodelist.getLength() > 0) {
            tmp = nodelist.item(0).getFirstChild().getNodeValue();
            try {
             value = DatatypeFactory.newInstance().newXMLGregorianCalendar(tmp);
            }
            catch (DatatypeConfigurationException exception) {
            }
        }
     return value;
    }
    
    public List<Cassonetto> daSvuotare() {
        ArrayList<Cassonetto> risultato = new ArrayList<>();
        XMLGregorianCalendar now;
        Duration H24;
        
        if (!parsed)
          return null;
        
        try {
         H24 = DatatypeFactory.newInstance().newDuration(-24*3600*1000);
         GregorianCalendar tmp = new GregorianCalendar();
         now = DatatypeFactory.newInstance().newXMLGregorianCalendar(tmp);
         now.add(H24);
        }
        catch (DatatypeConfigurationException exception) {
            return null;
        }
        
        Iterator iterator = cassonetti.iterator();
        while (iterator.hasNext()) {
             Cassonetto tmp = (Cassonetto)iterator.next();
             if (tmp.getTipo().equalsIgnoreCase("generico")) {
                 if (now.compare(tmp.getSvuotamento()) == DatatypeConstants.GREATER)
                     risultato.add(tmp);
             }
                 
        }
        return risultato;
    }
    
    //prende tutti i cassonetti
    public List<Cassonetto> getAll() {
        ArrayList<Cassonetto> risultato = new ArrayList<>();
        XMLGregorianCalendar now;
        Duration H24;
        
        if (!parsed)
          return null;
        
        try {
         H24 = DatatypeFactory.newInstance().newDuration(-24*3600*1000);
         GregorianCalendar tmp = new GregorianCalendar();
         now = DatatypeFactory.newInstance().newXMLGregorianCalendar(tmp);
         now.add(H24);
        }
        catch (DatatypeConfigurationException exception) {
            return null;
        }
        
        Iterator iterator = cassonetti.iterator();
        while (iterator.hasNext()) {
            Cassonetto tmp = (Cassonetto)iterator.next();
            risultato.add(tmp);                 
        }
        return risultato;
    }
    
    public static void main(String[] args) throws DatatypeConfigurationException {
        List<Cassonetto> cassonetti = null;
        Cassonetto tmpC=null;
        Applicazione app = new Applicazione();
        try {
           
            app.parseDocument("documento.xml");
            //cassonetti = app.daSvuotare();
            cassonetti = app.getAll(); //prelevo tutti i cassonetti
            
            System.out.println("Numero di cassonetti da svuotare: " + cassonetti.size());
            Iterator iterator = cassonetti.iterator();
            while (iterator.hasNext()) {
                 System.out.println(iterator.next().toString());
            }
        }
        catch (ParserConfigurationException | SAXException | IOException exception) {
            System.out.println("Errore!");
        }
        //modifica
        
        /*
        GregorianCalendar c= new GregorianCalendar();
        c.setTime(new Date());
        try{
            XMLGregorianCalendar data2=DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
            tmpC= new Cassonetto("barzigola", "della banana", 57, 22.22, 22.22, "guarda di worcare", data2, data2);
            
        }catch(Exception e){
            e.printStackTrace();
        }
        
        //creazione file xml 
        cassonetti.add(tmpC);
        
        try {
            JAXBContext context= JAXBContext.newInstance(Cassonetto.class);
            Marshaller marshaller = context.createMarshaller(); 
            marshaller.marshal(cassonetti,new FileWriter("documento.xml"));
        } catch (JAXBException ex) {
            Logger.getLogger(Applicazione.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Applicazione.class.getName()).log(Level.SEVERE, null, ex);
        }
        */
    }
}
