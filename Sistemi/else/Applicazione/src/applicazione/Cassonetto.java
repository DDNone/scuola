
package applicazione;

import javax.xml.datatype.*;
import java.sql.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Cassonetto")
@XmlAccessorType(XmlAccessType.FIELD)

public class Cassonetto {
    private String tipo;
    private String via;
    private int civico;
    private double latitudine;
    private double longitudine; 
    private String note;
    private XMLGregorianCalendar posizionamento;
    private XMLGregorianCalendar svuotamento;

    public Cassonetto() {
    }

    public Cassonetto(String tipo, String via, int civico, double latitudine, double longitudine, String note, XMLGregorianCalendar posizionamento, XMLGregorianCalendar svuotamento) {
        this.tipo = tipo;
        this.via = via;
        this.civico = civico;
        this.latitudine = latitudine;
        this.longitudine = longitudine;
        this.note = note;
        this.posizionamento = posizionamento;
        this.svuotamento = svuotamento;
    }
    
    public Cassonetto(Cassonetto cassonetto) {
        tipo = cassonetto.getTipo();
        via = cassonetto.getVia();
        civico = cassonetto.getCivico();
        latitudine = cassonetto.getLatitudine();
        longitudine = cassonetto.getLongitudine();
        note = cassonetto.getNote();
        posizionamento = cassonetto.getPosizionamento();
        svuotamento = cassonetto.getSvuotamento();
    }

    public String getTipo() {
        return tipo;
    }

    public String getVia() {
        return via;
    }

    public int getCivico() {
        return civico;
    }

    public double getLatitudine() {
        return latitudine;
    }

    public double getLongitudine() {
        return longitudine;
    }

    public String getNote() {
        return note;
    }

    public XMLGregorianCalendar getPosizionamento() {
        return posizionamento;
    }

    public XMLGregorianCalendar getSvuotamento() {
        return svuotamento;
    }
    
    @Override
    public String toString() {
        return "Cassonetto{" + "tipo=" + tipo + ", via=" + via + ", civico=" + civico + ", latitudine=" + latitudine + ", longitudine=" + longitudine + ", note=" + note + ", posizionamento=" + posizionamento + ", svuotamento=" + svuotamento + '}';
    }
}
