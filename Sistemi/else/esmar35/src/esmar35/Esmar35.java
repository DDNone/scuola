package esmar35;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

public class Esmar35 {
    public static void main(String[] args) throws FileNotFoundException {
        // TODO code application logic here
        StazioniMetereologiche stm;
        try {
            JAXBContext context= JAXBContext.newInstance("esmar35");
            Unmarshaller unmarshaller= context.createUnmarshaller(); 
            stm=(StazioniMetereologiche)unmarshaller.unmarshal(new FileReader("//websrv/alunni/INFO/CorsoDinftel-Pr2014/Alessio Tommasi/scuola/tps/ES 35/documento35.xml"));
            
            Grafica frame=new Grafica();
            
            System.out.println("stmpa: "+stm.toString());
            
            
        } catch (JAXBException ex) {
            System.out.println("barzigola ");
            Logger.getLogger(Esmar35.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
