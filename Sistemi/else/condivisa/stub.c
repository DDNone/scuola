
#include <stdio.h>
#include <string.h>

#include "com.h"

static HANDLE handle = NULL;

static int lat_deg = 46;
static float lat_min = 8.28;
static int lon_deg = 9;
static float lon_min = 23.1;

static int day = 28;
static int month = 1;
static int year = 2019;
static int hour = 8;
static int minute = 32;
static int second = 18;


HANDLE COM_open(unsigned char port, unsigned char mode, unsigned short speed, char parity, unsigned char bits, unsigned char stop, unsigned char flow)
{
 if (port < 1 || port > 9)
   return NULL;

 handle = (HANDLE)(99);
 return handle;
}

int COM_write(HANDLE com, char buf[], int n)
{
 if (com != handle)
   return -1;
 return 0;
}

int COM_read(HANDLE com, char *buf, int n)
{
 char NMEA_string[128], NMEA_crc[8];
 unsigned char crc = 0x00;
 int i;

 if (com != handle)
   return -1;
 
 sprintf(NMEA_string, "$GPRMC,%02i%02i%02i,A,%02i0%02.4f,N,%03i%02.4f,E,0.00,000.00,%02i%02i%02i*",
	     hour,minute,second,
		 lat_deg,lat_min,
		 lon_deg,lon_min,
		 day,month,(year-2000));
 
 for (i=1; NMEA_string[i]!='*'; i++)
    crc ^= NMEA_string[i];
 sprintf(NMEA_crc,"%02X\r\n",crc);
 strcpy(buf, NMEA_string);
 strcat(buf, NMEA_crc);

 return strlen(buf);
}

void COM_close(HANDLE com)
{
 handle = NULL;
 return;
} 
