package calendarquickstart;
public class Impegno {
    String nome,descrizione,inizio,fine,id;

    public Impegno(String nome, String descrizione, String inizio, String fine) {
        this.nome = nome;
        this.descrizione = descrizione;
        this.inizio = inizio;
        this.fine = fine;
    }

    public Impegno(String nome, String descrizione, String inizio, String fine, String id) {
        this.nome = nome;
        this.descrizione = descrizione;
        this.inizio = inizio;
        this.fine = fine;
        this.id = id;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public void setInizio(String inizio) {
        this.inizio = inizio;
    }

    public void setFine(String fine) {
        this.fine = fine;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public String getInizio() {
        return inizio;
    }

    public String getFine() {
        return fine;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Impegno{" + "nome=" + nome + ", descrizione=" + descrizione + ", inizio=" + inizio + ", fine=" + fine + ", id=" + id + '}';
    }
    
    public void toStringId()
    {
        System.out.println("id Evento: " + id);
    }
    
}
