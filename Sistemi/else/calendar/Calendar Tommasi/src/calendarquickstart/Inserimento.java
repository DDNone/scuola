package calendarquickstart;

import static calendarquickstart.Main.service;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventDateTime;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;  
import java.util.Date;  

public class Inserimento {

    Inserimento(Impegno impegno) throws IOException, ParseException {

        Event event = new Event()
                .setSummary(impegno.getNome())
                .setDescription(impegno.getDescrizione());
        Date inizio=new SimpleDateFormat("dd/MM/yyyy").parse(impegno.getInizio());
        Date fine=new SimpleDateFormat("dd/MM/yyyy").parse(impegno.getFine());    
        
        try {
            DateTime startDateTime = new DateTime(inizio);
            EventDateTime start = new EventDateTime()
                    .setDateTime(startDateTime);
            event.setStart(start);

            DateTime endDateTime = new DateTime(fine);
            EventDateTime end = new EventDateTime()
                    .setDateTime(endDateTime);
            event.setEnd(end);

            String calendarId = "primary";
            event = service.events().insert(calendarId, event).execute();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    Inserimento() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
