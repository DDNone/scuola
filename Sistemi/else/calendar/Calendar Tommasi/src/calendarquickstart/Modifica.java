package calendarquickstart;

import static calendarquickstart.Main.service;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventDateTime;
import java.io.IOException;

public class Modifica {
    Modifica(Impegno impegno) throws IOException
    {
        
        Event event = new Event()
                .setSummary(impegno.getNome())
                .setDescription(impegno.getDescrizione());

        DateTime startDateTime = new DateTime(impegno.getInizio());
        EventDateTime start = new EventDateTime()
                .setDateTime(startDateTime);
        event.setStart(start);

        DateTime endDateTime = new DateTime(impegno.getFine());
        EventDateTime end = new EventDateTime()
                .setDateTime(endDateTime);
        event.setEnd(end);
        
        String calendarId = "primary";
        Event updatedEvent = service.events().update(calendarId, impegno.getId(), event).execute();

    }
}
