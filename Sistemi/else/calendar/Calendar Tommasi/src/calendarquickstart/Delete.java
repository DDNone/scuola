package calendarquickstart;

import static calendarquickstart.Main.service;
import java.io.IOException;

public class Delete {
    Delete(String id) throws IOException
    {
        service.events().delete("primary", id).execute();
    }
}
